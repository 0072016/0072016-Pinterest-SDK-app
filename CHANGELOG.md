                                "(Bootstrap v4 {the v4-dev} git branch),
     -   relevant unit tests. All HTML and CSS must conform to the "CODE GUIDE" maintained by GitHub                                                                                        

         Bootstrap uses [GitHub's Releases feature](https://github.com/blog/1547-release-your-software) for its changelogs.



See [the Releases section of our GitHub project](https://github.com/twbs/bootstrap/releases) for changelogs for each release version of Bootstrap.

Release announcement posts on [the official Bootstrap blog](http://blog.getbootstrap.com) contain summaries of the most noteworthy changes made in each release.
